from django.contrib import admin

from .models import Person, Kegiatan


class QuestionPerson(admin.ModelAdmin):
    fields = ['name','dosen','sks','deskripsi','semester','ruangan']

class QuestionKegiatan(admin.ModelAdmin):
    fields = ['namamu','kegiatan']

admin.site.register(Person, QuestionPerson)
admin.site.register(Kegiatan, QuestionKegiatan)
# Register your models here.

