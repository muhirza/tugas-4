from django.urls import path, include
from .views import contact,portofolio,form,savename,readperson,delete,detil,readhapus, formkegi, savekegi, Kegiatan, qna

app_name = "tugas4"
urlpatterns=[
    path('contact/',contact,name='contact'),
    path('portofolio/',portofolio,name='portofolio'),
    #path('matkul/',savename,name='savename')
    path('form/',form,name='form'),
    path('form/savename',savename),
    path('hasil/',readperson,name='hasil'),
    path('hasilhapus/',readhapus,name='hasilhapus'),
    path('hapus/<id>/',delete,name='delete'),
    path('detil/<id>/',detil,name='detil'),
    path('formkegiatan/',formkegi,name='formkegi'),
    path('formkegiatan/savekegi',savekegi),
    path('kegiatan/',Kegiatan),
    path('QNA/',qna, name ='QNA')
]