from django.shortcuts import render
from .forms import Input_Form, CHOICES,dd_form 
from django.http import HttpResponseRedirect
from .models import Person,Kegiatan

# Create your views here.

def contact(request):
    return render(request,"contact.html")
def portofolio(request):
    return render(request,"portofolio.html")
def savename(request):
    form = Input_Form(request.POST or None)
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/form')
    else:
        return HttpResponseRedirect('/form')
def form(request):
    response={ 'input_form':Input_Form}
    return render(request,'form.html',response)

def readperson(request):
    objs=Person.objects.all()
    html = 'hasil.html'
    
    return render(request, html, {'objs':objs})

def readhapus(request):
    objs=Person.objects.all()
    html = 'hasilhapus.html'
    
    return render(request, html, {'objs':objs})

def delete(request,id):
    dele=Person.objects.get(id=id).delete()
    return render(request,'hapus.html',{'dele':dele,'Person':Person})

def detil(request,id):
    obj=Person.objects.get(id=id)
    
    return render(request,'detil.html',{'obj':obj,'Person':Person})

def formkegi(request):
    response={ 'dd_form':dd_form}
    return render(request,'formkegiatan.html',response)

def savekegi(request):
    form = dd_form(request.POST or None)
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/kegiatan')
    else:
        return HttpResponseRedirect('/kegiatan')
def Kegiatan(request,kegiatan):
    musik=Kegiatan.objects.get(kegiatan=="musik")
    mabar=Kegiatan.objects.get(kegiatan=="mabar")
    tidur=Kegiatan.objects.get(kegiatan=="tidur")
    
    return render(request,'kegiatan.html',{'musik':musik,'mabar':mabar,'tidur''Kegiatan':Kegiatan})

def qna(request):
    return render(request,"QNA.html")