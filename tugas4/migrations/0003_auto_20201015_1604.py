# Generated by Django 3.1.1 on 2020-10-15 09:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tugas4', '0002_person_dosen'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='deskripsi',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='person',
            name='sks',
            field=models.IntegerField(blank=True, default=9),
        ),
    ]
