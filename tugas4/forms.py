from django import forms
from .models import Person,Kegiatan

CHOICES=[
    ('musik','musik'),('mabar','mabar'),("tidur","tidur"),
]

class Input_Form(forms.ModelForm):
    class Meta:
        model = Person  
        fields =['name','dosen','sks','deskripsi','semester','ruangan']
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'your name'

    }
    name = forms.CharField(label='Mata Kuliah:', required=True)
    dosen = forms.CharField(label='Nama Dosen:', required=True)
    sks = forms.IntegerField(label='SKS:', required=True, min_value=1, max_value=6)
    deskripsi = forms.CharField(required=False)
    semester = forms.CharField(label='Semester:', required=True)
    ruangan = forms.CharField(label='Ruang:', required=True, )


class dd_form(forms.ModelForm):
    namamu = forms.CharField(label='Nama:', required=True)
    kegiatan = forms.CharField(label='Kegiatan: ', widget=forms.Select(choices=CHOICES))
    class Meta:
        model = Kegiatan
        fields =['namamu','kegiatan']
    
