from django.db import models

# Create your models here.

class Person(models.Model):
    #Person.objects.filter(id='sks').delete()
    name =models.CharField(max_length=200)
    dosen = models.CharField(max_length=200, default='')
    sks = models.IntegerField(blank=True, default=1)
    deskripsi = models.CharField(max_length=200, default='')
    semester = models.CharField(max_length=200, default='')
    ruangan = models.CharField(max_length=200, default='')
class Kegiatan(models.Model):
    namamu=  models.CharField(max_length=50, default='')
    kegiatan = models.CharField(max_length=50, default='')
    